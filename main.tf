resource "aws_security_group" "dm_sg_res" {
  name        = "${var.env_name}-server-sg"
  description = "multiple firewall rules"
  vpc_id      = var.acct_vpc_id

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

}

resource "aws_instance" "server_inst_res" {
  ami                    = var.aws_ami
  instance_type          = var.server_type
  vpc_security_group_ids = [aws_security_group.dm_sg_res.id]
  key_name               = var.target_keypairs
  subnet_id              = var.acct_subnet_id

  tags = {
    Name = "${var.env_name}-server"
  }
}

output "pub_ip" {
  value = ["${aws_instance.server_inst_res.public_ip}"]
}